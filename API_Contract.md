# API Contract

Main Map : **/api/v1/**



## Authentication

### User

#### 1.	POST /auth/login

- **Purpose** : login

- **URL Params**
  none

- **Data Params**

  ```
  {
    	username: <username>
    	password: <password>, 
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Login Successfully"
      data :{
      			fullname:
      			username:
      			email:
      			phone:
      			role:
      			status:
      			created_at:
      			updated_at:
      			activated_at:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "User doesn't exist"
      data :[]
    }`
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.	POST /auth/register

- **Purpose** : register

- **URL Params**
  none

- **Data Params**

  ```
  {
  	username: <username>,
    	email: <email>,
    	password: <password>, 
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Register Successfully"
      data :{
      			fullname:
      			username:
      			email:
      			phone:
      			role:
      			status:
      			createdAt:
      			updatedAt:
      			activatedAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "User already exist"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

## Manage Stock/ Product

### Product

#### 1.	GET /products

- **Purpose** : View All products

- **URL Params**
  none

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "List of All Products"
      data :[{
      			id:
      			name:
      			stock:
      			price:
      			priceOfDozen:
      			description:
      			image:
      			createdAt:
      			updatedAt:
      			deletedAt:
      			category: {
      					id:
      					name:
      			}
      		},
      		]
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "List of All Products not found"
      data :[]
    }`
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.	GET /product/:id

- **Purpose** : view product by id

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Detail Product"
      data :{
      			id:
      			name:
      			stock:
      			price:
      			priceOfDozen:
      			description:
      			image:
      			createdAt:
      			updatedAt:
      			deletedAt:
      			category: {
      					id:
      					name:
      			}
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "Detail Product is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 3.	POST  /product

- **Purpose** : add product

- **URL Params**
  none

- **Data Params**

  ```
  {
    	name:
    	stock:
    	price:
    	priceOfDozen:
    	description:
    	image:
    	category: {
    				id:
  			  }
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Add Product Successfully"
      data :{
      			id:
      			name:
      			stock:
      			price:
      			priceOfDozen:
      			description:
      			image:
      			createdAt:
      			updatedAt:
      			deletedAt:
      			category: {
      					id:
      					name:
      			}
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Add Product Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 4.	PUT /product/:id

- **Purpose** : update product

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  ```
  {
    	name:
    	stock:
    	price:
    	priceOfDozen:
    	description:
    	image:
    	category: {
    				id:
  			  }
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Update Product Successfully"
      data :{
      			id:
      			name:
      			stock:
      			price:
      			priceOfDozen:
      			description:
      			image:
      			createdAt:
      			updatedAt:
      			deletedAt:
      			category: {
      					id:
      					name:
      			}
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Update Product Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 5.	DELETE /product/:id

- **Purpose** : Delete product

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Delete Product Successfully"
      data :{
      			id:
      			name:
      			stock:
      			price:
      			priceOfDozen:
      			description:
      			image:
      			createdAt:
      			updatedAt:
      			deletedAt:
      			category: {
      					id:
      					name:
      			}
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Delete Product Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    



## Manage Category

### Category

#### 1.	GET /categories

- **Purpose** : View All categories

- **URL Params**
  none

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "List of All Categories"
      data :[{
      			id:
      			name:
      		},
      		]
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "List of All Categories not found"
      data :[]
    }`
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.	GET /category/:id

- **Purpose** : view category by id

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Detail Category"
      data :{
      			id:
      			name:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "Detail Category is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 3.	POST  /category

- **Purpose** : add product

- **URL Params**
  none

- **Data Params**

  ```
  {
    	name:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Add Category Successfully"
      data :{
      			id:
      			name:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Add Category Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 4.	PUT /category/:id

- **Purpose** : update category

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  ```
  {
    	name:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Update Category Successfully"
      data :{
      			id:
      			name:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Update Category Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 5.	DELETE /category/:id

- **Purpose** : Delete product

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Delete Category Successfully"
      data :{
      			id:
      			name:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Delete Category Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

## Manage Users

### User

#### 1.	GET /users

- **Purpose** : View All users

- **URL Params**
  none

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "List of All Users"
      data :[{
      			id:
      			fullname:
      			username:
      			email:
      			address:
      			phone:
      			profileImage:
      			role:
      			status:
      			createdAt:
      			updateAt:
      			activatedAt:			
      			
      		},
      		]
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "List of All Users not found"
      data :[]
    }`
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.	GET /user/:id

- **Purpose** : view user by id

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Detail User"
      data :{
      			id:
      			fullname:
      			username:
      			email:
      			address:
      			phone:
      			profileImage:
      			role:
      			status:
      			createdAt:
      			updateAt:
      			activatedAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "Detail User is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 3.	POST  /user

- **Purpose** : add product

- **URL Params**
  none

- **Data Params**

  ```
  {
    	fullname:
    	username:
    	email:
    	address:
    	phone:
    	profileImage:
    	role:
    	status:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Add User Successfully"
      data :{
      			id:
      			fullname:
      			username:
      			email:
      			address:
      			phone:
      			profileImage:
      			role:
      			status:
      			createdAt:
      			updateAt:
      			activatedAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Add User Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 4.	PUT /user/:id

- **Purpose** : update category

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  ```
  {
    	fullname:
    	username:
    	email:
    	address:
    	phone:
    	profileImage:
    	role:
    	status:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Update User Successfully"
      data :{
      			id:
      			fullname:
      			username:
      			email:
      			address:
      			phone:
      			profileImage:
      			role:
      			status:
      			createdAt:
      			updateAt:
      			activatedAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Update User Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 5.	PUT /user/password/:id

- **Purpose** : change password

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  ```
  {
    	id:
    	password:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Change Password User Successfully"
      data :{
      			id:
      			fullname:
      			username:
      			email:
      			address:
      			phone:
      			profileImage:
      			role:
      			status:
      			createdAt:
      			updateAt:
      			activatedAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Change Password User Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    



#### 6.	DELETE /user/:id

- **Purpose** : Delete product

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Delete User Successfully"
      data :{
      			id:
      			fullname:
      			username:
      			email:
      			address:
      			phone:
      			profileImage:
      			role:
      			status:
      			createdAt:
      			updateAt:
      			activatedAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Delete User Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

## Manage Transaction

### 1.Transaction

#### 1.	GET /transactions

- **Purpose** : View All transactions

- **URL Params**
  none

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "List of All Transactions"
      data :[{
      			id:
      			user:{
    					id:
      					fullname:
      					username:
      					email:
      					address:
      					phone:
      					role:
      					status:
      				}
      			gross:
      			discount:
      			invoiceNumber:
      			status:
      			paymentDate:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		},
      		]
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "List of All Transactions not found"
      data :[]
    }`
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.	GET /transaction/:id

- **Purpose** : view transcation by id

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Transaction"
      data :{
      			id:
      			user:{
      					id:
      					fullname:
      					username:
      					email:
      					address:
      					phone:
      					role:
      					status:
      				}
      			gross:
      			discount:
      			invoiceNumber:
      			status:
      			paymentDate:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "Transaction is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2A.	GET /transaction/status/:status

- **Purpose** : view transcation by status

- **URL Params**
  *Required:* `status=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "List of Transaction By Status"
      data :{
      			id:
      			user:{
      					id:
      					fullname:
      					username:
      					email:
      					address:
      					phone:
      					role:
      					status:
      				}
      			gross:
      			discount:
      			invoiceNumber:
      			status:
      			paymentDate:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "List of Transaction By Status	 is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 3.	POST  /transaction

- **Purpose** : add product

- **URL Params**
  none

- **Data Params**

  ```
  {
    	user:{
    			id:
    			}
    	gross:
    	discount:
    	invoiceNumber:
    	status:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Add Transaction Successfully"
      data :{
      			id:
      			user:{
      					id:
      					fullname:
      					username:
      					email:
      					address:
      					phone:
      					role:
      					status:
      				}
      			gross:
      			discount:
      			invoiceNumber:
      			status:
      			paymentDate:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Add Transcaction Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 4.	PUT /transaction/:id

- **Purpose** : update transaction

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  ```
  {
    	gross:
    	discount:
    	invoiceNumber:
    	status:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Update Transaction Successfully"
      data :{
      			id:
      			user:{
      					id:
      					fullname:
      					username:
      					email:
      					address:
      					phone:
      					role:
      					status:
      				}
      			gross:
      			discount:
      			invoiceNumber:
      			status:
      			paymentDate:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Update Transaction Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 5.	DELETE /transaction/:id

- **Purpose** : Delete transaction

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Delete Transaction Successfully"
      data :{
      			id:
      			user:{
      					id:
      					fullname:
      					username:
      					email:
      					address:
      					phone:
      					role:
      					status:
      				}
      			gross:
      			discount:
      			invoiceNumber:
      			status:
      			paymentDate:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Delete Transaction Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

### 2. Transaction_Detail

#### 1.	GET /transaction/details

- **Purpose** : View All transactions detail

- **URL Params**
  none

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "List of All Detail Transactions"
      data :[{
      			id:
      			transaction:{
                                id:
                                paymentDate:
                                status:
      				}
      			product:{
      						id:
      						name:
      			}
      			price:
      			quantity:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		},
      		]
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "List of All Transactions not found"
      data :[]
    }`
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.	GET /transaction/:id/details/

- **Purpose** : view transcation detail by id

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Transaction Detail by Transaction"
      data :[{
      			id:
      			transaction:{
                                id:
                                paymentDate:
                                status:
      				}
      			product:{
      						id:
      						name:
      			}
      			price:
      			quantity:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		},]
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "Transaction Detail is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 2.A GET /transaction/detail/:id

- **Purpose** : view transcation detail by id

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Transaction Detail by Transaction"
      data :{
      			id:
      			transaction:{
                                id:
                                paymentDate:
                                status:
      				}
      			product:{
      						id:
      						name:
      			}
      			price:
      			quantity:
      			createdAt:
      			upadteAt:
      			deleteAt:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 404
    **Content:** 

    ```
    {
      status : 404,
      error : "Transaction Detail by Transaction is not found"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 3.	POST  /transaction/detail

- **Purpose** : add product

- **URL Params**
  none

- **Data Params**

  ```
  {
    	transaction:{
    					id:
    				}
    	product:{
    				id:
    	}
    	price:
    	quantity:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Add Transaction Detail Successfully"
      data :{
      			id:
      			transaction:{
      					id:
      				}
                product:{
                            id:
                }
                price:
                quantity:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Add Transcaction Detail Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 4.	PUT /transaction/detail/:id

- **Purpose** : update transaction

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  ```
  {
    	transaction:{
    					id:
    				}
    	product:{
    				id:
    	}
    	price:
    	quantity:
  }
  ```

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Update Transaction Detail Successfully"
      data :{
      			id:
      			transaction:{
      					id:
      				}
                product:{
                            id:
                }
                price:
                quantity:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Update Transaction Detail Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

#### 5.	DELETE /transaction/detail/:id

- **Purpose** : Delete transaction

- **URL Params**
  *Required:* `id=[integer]`

- **Data Params**

  none

- **Headers**
  Content-Type: application/json

- **Success Response:**

  - **Code:** 200
    **Content:**

    ```
    {
      status : 200,
      message : "Delete Transaction Successfully"
      data :{
      			id:
      			transaction:{
      					id:
      				}
                product:{
                            id:
                }
                price:
                quantity:
      		}
    }
    ```

    

- **Error Response:**

  - **Code:** 400
    **Content:** 

    ```
    {
      status : 400,
      error : "Delete Transaction Failed"
      data :[]
    }
    ```

    

  - **Code:** 500
    **Content:** 

    ```
    {
      status : 500,
      error : "Internal Server Error",
      data :[]
    } 
    ```

    

## **GET /users/:id**

- 
